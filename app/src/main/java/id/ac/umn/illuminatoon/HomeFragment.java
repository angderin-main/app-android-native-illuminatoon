package id.ac.umn.illuminatoon;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class HomeFragment extends Fragment {

    private int height,width;

    JSONParser jsonParser = new JSONParser();
    ArrayList<HashMap<String, String>> recomendedList = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> newestList = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> rankList = new ArrayList<HashMap<String, String>>();

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_RECOMENDED = "recomended";
    private static final String TAG_NEWEST = "newest";
    private static final String TAG_RANK = "rank";
    private static final String TAG_ID_KOMIK = "id_komik";
    private static final String TAG_NAMA_KOMIK = "nama_komik";
    private static final String TAG_DESC = "desc";
    private static final String TAG_STATUS = "status";
    private static final String TAG_NAMA_PENULIS = "nama_penulis";
    private static final String TAG_TANGGAL_TERBIT = "tanggal_terbit";
    private static final String TAG_LIKE = "like_komik";
    private static final String TAG_URL_PROFILE = "url_profile";

    JSONArray recomended,newest,rank = null;

    View rootView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        rootView = inflater.inflate(R.layout.fragment_home,container,false);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity)getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;
        height = displayMetrics.heightPixels;

        //new LongOperation().execute();

        CarouselPagerAdapter carouselPagerAdapter = new CarouselPagerAdapter(getChildFragmentManager());
        ViewPager carouselPager = (ViewPager) rootView.findViewById(R.id.carousel);
        carouselPager.setAdapter(carouselPagerAdapter);

        recomendedList = ArrayComicClass.getRecomendedList();
        newestList = ArrayComicClass.getNewestList();
        rankList = ArrayComicClass.getRankList();

        updateUI();



        return rootView;
    }

    public void updateUI(){

        ImageView recomeded_image_1 = (ImageView)rootView.findViewById(R.id.image_recomended_1);
        ImageView recomeded_image_2 = (ImageView)rootView.findViewById(R.id.image_recomended_2);
        ImageView recomeded_image_3 = (ImageView)rootView.findViewById(R.id.image_recomended_3);

        TextView recomended_nama_komik_1 = (TextView)rootView.findViewById(R.id.nama_komik_recomended_1);
        TextView recomended_nama_komik_2 = (TextView)rootView.findViewById(R.id.nama_komik_recomended_2);
        TextView recomended_nama_komik_3 = (TextView)rootView.findViewById(R.id.nama_komik_recomended_3);

        TextView recomended_desc_1 = (TextView)rootView.findViewById(R.id.desc_recomended_1);
        TextView recomended_desc_2 = (TextView)rootView.findViewById(R.id.desc_recomended_2);
        TextView recomended_desc_3 = (TextView)rootView.findViewById(R.id.desc_recomended_3);

        TableRow recomended_row_1 = (TableRow)rootView.findViewById(R.id.row_recomended_1);
        TableRow recomended_row_2 = (TableRow)rootView.findViewById(R.id.row_recomended_2);
        TableRow recomended_row_3 = (TableRow)rootView.findViewById(R.id.row_recomended_3);

        recomended_row_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),ChapterActivity.class);
                intent.putExtra("id_komik",recomendedList.get(0).get(TAG_ID_KOMIK));
                intent.putExtra("url_profile",recomendedList.get(0).get(TAG_URL_PROFILE));
                intent.putExtra("desc",recomendedList.get(0).get(TAG_DESC));
                intent.putExtra("nama_komik",recomendedList.get(0).get(TAG_NAMA_KOMIK));
                intent.putExtra("nama_penulis",recomendedList.get(0).get(TAG_NAMA_PENULIS));
                intent.putExtra("tanggal_terbit",recomendedList.get(0).get(TAG_TANGGAL_TERBIT));
                intent.putExtra("like",recomendedList.get(0).get(TAG_LIKE));
                startActivity(intent);
            }
        });

        recomended_row_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),ChapterActivity.class);
                intent.putExtra("id_komik",recomendedList.get(1).get(TAG_ID_KOMIK));
                intent.putExtra("url_profile",recomendedList.get(1).get(TAG_URL_PROFILE));
                intent.putExtra("desc",recomendedList.get(1).get(TAG_DESC));
                intent.putExtra("nama_komik",recomendedList.get(1).get(TAG_NAMA_KOMIK));
                intent.putExtra("nama_penulis",recomendedList.get(1).get(TAG_NAMA_PENULIS));
                intent.putExtra("tanggal_terbit",recomendedList.get(1).get(TAG_TANGGAL_TERBIT));
                intent.putExtra("like",recomendedList.get(1).get(TAG_LIKE));
                startActivity(intent);
            }
        });

        recomended_row_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),ChapterActivity.class);
                intent.putExtra("id_komik",recomendedList.get(2).get(TAG_ID_KOMIK));
                intent.putExtra("url_profile",recomendedList.get(2).get(TAG_URL_PROFILE));
                intent.putExtra("desc",recomendedList.get(2).get(TAG_DESC));
                intent.putExtra("nama_komik",recomendedList.get(2).get(TAG_NAMA_KOMIK));
                intent.putExtra("nama_penulis",recomendedList.get(2).get(TAG_NAMA_PENULIS));
                intent.putExtra("tanggal_terbit",recomendedList.get(2).get(TAG_TANGGAL_TERBIT));
                intent.putExtra("like",recomendedList.get(2).get(TAG_LIKE));
                startActivity(intent);
            }
        });

        Glide.with(this)
                .load(recomendedList.get(0).get(TAG_URL_PROFILE))
                .into(recomeded_image_1);
        Glide.with(this)
                .load(recomendedList.get(1).get(TAG_URL_PROFILE))
                .into(recomeded_image_2);
        Glide.with(this)
                .load(recomendedList.get(2).get(TAG_URL_PROFILE))
                .into(recomeded_image_3);

        recomended_nama_komik_1.setText(recomendedList.get(0).get(TAG_NAMA_KOMIK));
        recomended_nama_komik_2.setText(recomendedList.get(1).get(TAG_NAMA_KOMIK));
        recomended_nama_komik_3.setText(recomendedList.get(2).get(TAG_NAMA_KOMIK));

        recomended_desc_1.setText(recomendedList.get(0).get(TAG_DESC));
        recomended_desc_2.setText(recomendedList.get(1).get(TAG_DESC));
        recomended_desc_3.setText(recomendedList.get(2).get(TAG_DESC));

        LinearLayout linearLayout = (LinearLayout) rootView.findViewById(R.id.newest_list);

        int count = 0;

        for (int i=0; i<((int) Math.ceil(newestList.size()/2.0)); i++){
            TableRow tableRow = new TableRow(getContext());
            TableLayout.LayoutParams tableRowParams = new TableLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    (int) getResources().getDimension(R.dimen.height_recomended_row)
            );
            tableRowParams.setMargins(
                    0,
                    (int)getResources().getDimension(R.dimen.padding_recomended_row),
                    0,
                    (int)getResources().getDimension(R.dimen.padding_recomended_row)
            );
            tableRow.setLayoutParams(tableRowParams);
            tableRow.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            for (int x=0; x<2; x++,count++){
                //komikList.get(count).get(TAG_NAMA_KOMIK);
                if (count==newestList.size())break;
                LinearLayout linearLayout2 = new LinearLayout(getContext());
                linearLayout2.setLayoutParams(new TableRow.LayoutParams(
                        0,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        1
                ));
                linearLayout2.setOrientation(LinearLayout.VERTICAL);
                linearLayout2.setGravity(Gravity.CENTER);
                linearLayout2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                FrameLayout frameLayout = new FrameLayout(getContext());
                frameLayout.setLayoutParams(new LinearLayout.LayoutParams(
                        (int)getResources().getDimension(R.dimen.height_recomended_row),
                        (int)getResources().getDimension(R.dimen.height_recomended_row)
                ));

                ImageView profileImage = new ImageView(getContext());
                profileImage.setLayoutParams(new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                ));
                //profileImage.setId(Integer.parseInt(komikList.get(count).get(TAG_ID_KOMIK)));
                Glide.with(getContext())
                        .load(newestList.get(count).get(TAG_URL_PROFILE))
                        .into(profileImage);

                ImageView blackOverlay = new ImageView(getContext());
                blackOverlay.setLayoutParams(new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                ));
                blackOverlay.setImageDrawable(getResources().getDrawable(R.drawable.black_overlay));

                TextView nama_komik = new TextView(getContext());
                nama_komik.setLayoutParams(new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                ));
                nama_komik.setGravity(Gravity.CENTER|Gravity.BOTTOM);
                nama_komik.setPadding(0,0,0, (int) getResources().getDimension(R.dimen.padding_recomended_row));
                nama_komik.setTextColor(getResources().getColor(R.color.colorAccent));
                nama_komik.setText(newestList.get(count).get(TAG_NAMA_KOMIK));
                nama_komik.setId(count);
                nama_komik.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getContext(), Integer.toString(v.getId()), Toast.LENGTH_SHORT).show();
                        //Toast.makeText(getContext(), komikList.get(3).get(TAG_ID_KOMIK), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getContext(),ChapterActivity.class);
                        intent.putExtra("id_komik",newestList.get(v.getId()).get(TAG_ID_KOMIK));
                        intent.putExtra("url_profile",newestList.get(v.getId()).get(TAG_URL_PROFILE));
                        intent.putExtra("desc",newestList.get(v.getId()).get(TAG_DESC));
                        intent.putExtra("nama_komik",newestList.get(v.getId()).get(TAG_NAMA_KOMIK));
                        intent.putExtra("nama_penulis",newestList.get(v.getId()).get(TAG_NAMA_PENULIS));
                        intent.putExtra("tanggal_terbit",newestList.get(v.getId()).get(TAG_TANGGAL_TERBIT));
                        intent.putExtra("like",newestList.get(v.getId()).get(TAG_LIKE));
                        startActivity(intent);
                    }
                });

                frameLayout.addView(profileImage);
                frameLayout.addView(blackOverlay);
                frameLayout.addView(nama_komik);

                linearLayout2.addView(frameLayout);

                tableRow.addView(linearLayout2);
                //tableRow.addView(button);
            }

            linearLayout.addView(tableRow);
        }

        ImageView rank_image_1 = (ImageView)rootView.findViewById(R.id.rank_image_1);
        ImageView rank_image_2 = (ImageView)rootView.findViewById(R.id.rank_image_2);
        ImageView rank_image_3 = (ImageView)rootView.findViewById(R.id.rank_image_3);

        TextView rank_nama_komik_1 = (TextView)rootView.findViewById(R.id.rank_nama_komik_1);
        TextView rank_nama_komik_2 = (TextView)rootView.findViewById(R.id.rank_nama_komik_2);
        TextView rank_nama_komik_3 = (TextView)rootView.findViewById(R.id.rank_nama_komik_3);

        TextView rank_desc_1 = (TextView)rootView.findViewById(R.id.rank_desc_1);
        TextView rank_desc_2 = (TextView)rootView.findViewById(R.id.rank_desc_2);
        TextView rank_desc_3 = (TextView)rootView.findViewById(R.id.rank_desc_3);

        TableRow rank_row_1 = (TableRow)rootView.findViewById(R.id.rank_row_1);
        TableRow rank_row_2 = (TableRow)rootView.findViewById(R.id.rank_row_2);
        TableRow rank_row_3 = (TableRow)rootView.findViewById(R.id.rank_row_3);

        rank_row_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),ChapterActivity.class);
                intent.putExtra("id_komik",rankList.get(0).get(TAG_ID_KOMIK));
                intent.putExtra("url_profile",rankList.get(0).get(TAG_URL_PROFILE));
                intent.putExtra("desc",rankList.get(0).get(TAG_DESC));
                intent.putExtra("nama_komik",rankList.get(0).get(TAG_NAMA_KOMIK));
                intent.putExtra("nama_penulis",rankList.get(0).get(TAG_NAMA_PENULIS));
                intent.putExtra("tanggal_terbit",rankList.get(0).get(TAG_TANGGAL_TERBIT));
                intent.putExtra("like",rankList.get(0).get(TAG_LIKE));
                startActivity(intent);
            }
        });

        rank_row_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),ChapterActivity.class);
                intent.putExtra("id_komik",rankList.get(1).get(TAG_ID_KOMIK));
                intent.putExtra("url_profile",rankList.get(1).get(TAG_URL_PROFILE));
                intent.putExtra("desc",rankList.get(1).get(TAG_DESC));
                intent.putExtra("nama_komik",rankList.get(1).get(TAG_NAMA_KOMIK));
                intent.putExtra("nama_penulis",rankList.get(1).get(TAG_NAMA_PENULIS));
                intent.putExtra("tanggal_terbit",rankList.get(1).get(TAG_TANGGAL_TERBIT));
                intent.putExtra("like",rankList.get(1).get(TAG_LIKE));
                startActivity(intent);
            }
        });

        rank_row_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),ChapterActivity.class);
                intent.putExtra("id_komik",rankList.get(2).get(TAG_ID_KOMIK));
                intent.putExtra("url_profile",rankList.get(2).get(TAG_URL_PROFILE));
                intent.putExtra("desc",rankList.get(2).get(TAG_DESC));
                intent.putExtra("nama_komik",rankList.get(2).get(TAG_NAMA_KOMIK));
                intent.putExtra("nama_penulis",rankList.get(2).get(TAG_NAMA_PENULIS));
                intent.putExtra("tanggal_terbit",rankList.get(2).get(TAG_TANGGAL_TERBIT));
                intent.putExtra("like",rankList.get(2).get(TAG_LIKE));
                startActivity(intent);
            }
        });

        Glide.with(this)
                .load(rankList.get(0).get(TAG_URL_PROFILE))
                .into(rank_image_1);
        Glide.with(this)
                .load(rankList.get(1).get(TAG_URL_PROFILE))
                .into(rank_image_2);
        Glide.with(this)
                .load(rankList.get(2).get(TAG_URL_PROFILE))
                .into(rank_image_3);

        rank_nama_komik_1.setText(rankList.get(0).get(TAG_NAMA_KOMIK));
        rank_nama_komik_2.setText(rankList.get(1).get(TAG_NAMA_KOMIK));
        rank_nama_komik_3.setText(rankList.get(2).get(TAG_NAMA_KOMIK));

        rank_desc_1.setText(rankList.get(0).get(TAG_DESC));
        rank_desc_2.setText(rankList.get(1).get(TAG_DESC));
        rank_desc_3.setText(rankList.get(2).get(TAG_DESC));

    }

}
