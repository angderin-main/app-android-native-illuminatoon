package id.ac.umn.illuminatoon;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Derin Anggara on 6/13/2017.
 */

public class ArrayCarouselClass {

    private static ArrayList<HashMap<String, String>> carouselList2 = new ArrayList<HashMap<String, String>>();

    public static ArrayList<HashMap<String, String>> getCarouselList() {
        return carouselList2;
    }

    public static void setCarouselList(ArrayList<HashMap<String, String>> carouselList) {
        carouselList2 = carouselList;
    }
}
