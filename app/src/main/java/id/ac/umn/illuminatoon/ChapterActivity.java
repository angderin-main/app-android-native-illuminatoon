package id.ac.umn.illuminatoon;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ChapterActivity extends AppCompatActivity {

    JSONParser jsonParser = new JSONParser();
    ArrayList<HashMap<String, String>> chapterList = new ArrayList<HashMap<String, String>>();

    private static String url_chapter_komik = "http://192.168.1.10/kuliah/pemmob/illuminatoon/project/index.php/Api/getChapter";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_CHAPTER = "chapter";
    private static final String TAG_ID_KOMIK = "id_komik";
    private static final String TAG_ID_CHAPTER = "id_chapter";
    private static final String TAG_JUDUL_CHAPTER = "judul_chapter";
    private static final String TAG_LIKE_CHAPTER = "like_chapter";
    private static final String TAG_TANGGAL_CHAPTER = "tanggal_chapter";
    private static final String TAG_VIEW = "view";
    private static final String TAG_CHAPTER_IMAGE = "chapter_image";
    private static boolean UPDATED = false;

    JSONArray chapter = null;

    private String id_komik,url_profile,desc,nama_komik,nama_penulis,tanggal_terbit,like;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapter);

        id_komik = getIntent().getStringExtra("id_komik");
        url_profile = getIntent().getStringExtra("url_profile");
        desc = getIntent().getStringExtra("desc");
        nama_komik = getIntent().getStringExtra("nama_komik");
        nama_penulis = getIntent().getStringExtra("nama_penulis");
        tanggal_terbit = getIntent().getStringExtra("tanggal_terbit");
        like = getIntent().getStringExtra("like");

        ImageView profile_penulis = (ImageView) findViewById(R.id.profile_penulis);
        Glide.with(this)
                .load(url_profile)
                .into(profile_penulis);

        new LongOperation().execute();

    }

    public void updateUI(){

        int count = 0;

        TextView nama_komik = (TextView) findViewById(R.id.nama_komik);
        TextView desc_komik = (TextView) findViewById(R.id.desc_komik);
        TextView nama_penulis = (TextView) findViewById(R.id.nama_penulis);
        TextView like = (TextView) findViewById(R.id.like);
        TextView tanggal_terbit = (TextView) findViewById(R.id.tanggal_terbit);

        nama_komik.setText(this.nama_komik);
        desc_komik.setText(desc);
        nama_penulis.setText(this.nama_penulis);
        tanggal_terbit.setText(this.tanggal_terbit);
        like.setText(this.like);

        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.chapter_list);

        for (int i=0; i<chapterList.size(); i++){
            //Log.d("JUDUL CHAPTER",chapterList.get(0).get(TAG_JUDUL_CHAPTER));
            TableRow tableRow = new TableRow(this);
            tableRow.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            ));

            FrameLayout frameLayout = new FrameLayout(this);
            TableRow.LayoutParams frameLayoutParams = new TableRow.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    (int) getResources().getDimension(R.dimen.height_recomended_row)
            );
            frameLayoutParams.setMargins(
                    (int) getResources().getDimension(R.dimen.padding_recomended_row),
                    (int) getResources().getDimension(R.dimen.padding_recomended_row),
                    (int) getResources().getDimension(R.dimen.padding_recomended_row),
                    (int) getResources().getDimension(R.dimen.padding_recomended_row)
            );
            frameLayout.setLayoutParams(frameLayoutParams);
            frameLayout.setId(i);
            frameLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ChapterActivity.this,ReadActivity.class);
                    intent.putExtra("id_chapter",chapterList.get(v.getId()).get(TAG_ID_CHAPTER));
                    startActivity(intent);
                }
            });

            ImageView backgroundImage = new ImageView(this);
            backgroundImage.setLayoutParams(new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            ));
            //backgroundImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_dkv_kekar));
            Glide.with(this)
                    .load(url_profile)
                    .into(backgroundImage);
            backgroundImage.setScaleType(ImageView.ScaleType.CENTER_CROP);

            ImageView blackOverlay = new ImageView(this);
            blackOverlay.setLayoutParams(new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            ));
            blackOverlay.setImageDrawable(getResources().getDrawable(R.drawable.black_overlay));
            blackOverlay.setScaleType(ImageView.ScaleType.CENTER_CROP);

            TableRow tableRow1 = new TableRow(this);
            tableRow1.setLayoutParams(new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            ));

            ImageView chapterImage = new ImageView(this);
            chapterImage.setLayoutParams(new TableRow.LayoutParams(
                    (int) getResources().getDimension(R.dimen.height_recomended_row),
                    (int) getResources().getDimension(R.dimen.height_recomended_row)
            ));
            chapterImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_dkv_kekar));
            Glide.with(this)
                    .load(chapterList.get(i).get(TAG_CHAPTER_IMAGE))
                    .into(chapterImage);

            TextView judulChapter = new TextView(this);
            judulChapter.setLayoutParams(new TableRow.LayoutParams(
                    0,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    1
            ));
            judulChapter.setGravity(Gravity.CENTER);
            judulChapter.setTextColor(getResources().getColor(R.color.colorAccent));
            judulChapter.setTextSize(getResources().getDimension(R.dimen.judul_chapter));
            judulChapter.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            judulChapter.setText(chapterList.get(i).get(TAG_JUDUL_CHAPTER));

            tableRow1.addView(chapterImage);
            tableRow1.addView(judulChapter);

            frameLayout.addView(backgroundImage);
            frameLayout.addView(blackOverlay);
            frameLayout.addView(tableRow1);

            tableRow.addView(frameLayout);

            linearLayout.addView(tableRow);

        }

    }

    private class LongOperation extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            chapterList.clear();
        }

        @Override
        protected String doInBackground(String... params) {

            List<Pair<String, String>> args = new ArrayList<Pair<String, String>>();
            args.add(new Pair<String, String>(TAG_ID_KOMIK, id_komik));

            JSONObject jsonObject = null;

            try {
                jsonObject = jsonParser.makeHttpRequest(url_chapter_komik,"POST",args);
            } catch (IOException e) {
                //e.printStackTrace();
                Log.d("Networking", e.getLocalizedMessage());
            }
            try {
                int success = jsonObject.getInt(TAG_SUCCESS);
                if (success == 1){
                    chapter = jsonObject.getJSONArray(TAG_CHAPTER);

                    for (int i=0; i<chapter.length(); i++){
                        JSONObject c = chapter.getJSONObject(i);

                        String id_chapter = c.getString(TAG_ID_CHAPTER);
                        String judul_chapter = c.getString(TAG_JUDUL_CHAPTER);
                        String like_chapter = c.getString(TAG_LIKE_CHAPTER);
                        String tanggal_chapter = c.getString(TAG_TANGGAL_CHAPTER);
                        String view = c.getString(TAG_VIEW);
                        String chapter_image = c.getString(TAG_CHAPTER_IMAGE);

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID_CHAPTER,id_chapter);
                        map.put(TAG_JUDUL_CHAPTER,judul_chapter);
                        map.put(TAG_LIKE_CHAPTER,like_chapter);
                        map.put(TAG_TANGGAL_CHAPTER,tanggal_chapter);
                        map.put(TAG_VIEW,view);
                        map.put(TAG_CHAPTER_IMAGE,chapter_image);

                        //komikList.add(map);
                        chapterList.add(map);
                    }
                }else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String jsonArray) {
            super.onPostExecute(jsonArray);
            updateUI();
//            Intent i = new Intent(SplashScreenActivity.this,MainActivity.class);
//            i.putExtra("komik_list",komikList);
//            startActivity(i);
//            finish();
        }

    }
}
