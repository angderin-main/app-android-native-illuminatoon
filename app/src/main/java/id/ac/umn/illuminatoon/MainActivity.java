package id.ac.umn.illuminatoon;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArrayMap;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends FragmentActivity implements BottomNavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener {

    private ViewPager viewPager,carouselPager;
    private CarouselPagerAdapter carouselPagerAdapter;
    private BottomNavigationView navigation;
    private final static int INTERVAL = 1000 * 60 * 5;
    Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayList<HashMap<String, String>> arl = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("komik_list");
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.getData(arl);
        viewPager = (ViewPager)findViewById(R.id.viewPager);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOnPageChangeListener(this);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);

    }

    Runnable mHandlerTask = new Runnable()
    {
        @Override
        public void run() {
            mHandler.postDelayed(mHandlerTask, INTERVAL);
            new AsyncTask().startAsyncTask();
        }

        void startRepeatingTask()
        {
            mHandlerTask.run();
        }

        void stopRepeatingTask()
        {
            mHandler.removeCallbacks(mHandlerTask);
        }

    };

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.navigation_home :
                viewPager.setCurrentItem(0);
                return true;
            case R.id.navigation_listComic :
                viewPager.setCurrentItem(1);
                return true;
            case R.id.navigation_news :
                viewPager.setCurrentItem(2);
                return true;
//            case R.id.navigation_my :
//                viewPager.setCurrentItem(3);
//                return true;
            case R.id.navigation_more :
                viewPager.setCurrentItem(3);
                return true;
        }
        return false;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPageSelected(int position) {
        navigation.setSelectedItemId(navigation.getMenu().getItem(position).getItemId());
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
