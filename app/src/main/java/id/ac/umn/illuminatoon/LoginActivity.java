package id.ac.umn.illuminatoon;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private LinearLayout login_layout;
    private TableLayout profile_layout;
    private ImageView profile_image;
    private TextView profile_name, profile_email;
    private SignInButton login_button;
    private Button signout_button;
    private GoogleApiClient googleApiClient;
    private static final int REQ_CODE = 9001;

    private String name;
    private String email;
    private String profile_url;
    private String id_profile;


    JSONParser jsonParser = new JSONParser();
    //ArrayList<HashMap<String, String>> pageList = new ArrayList<HashMap<String, String>>();

    private static String url_check_user = id.ac.umn.illuminatoon.AsyncTask.base_url+"index.php/Api/check_user";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_ID_GOOGLE = "id_google";
    private static final String TAG_NAMA_PROFILE = "nama_profile";
    private static final String TAG_EMAIL_PROFILE = "email_profile";

    int response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login_layout = (LinearLayout) findViewById(R.id.layout_login);
        profile_layout = (TableLayout) findViewById(R.id.profile_layout);
        profile_image = (ImageView) findViewById(R.id.profile_image);
        profile_name = (TextView) findViewById(R.id.profile_nama);
        profile_email = (TextView) findViewById(R.id.profile_email);
        login_button = (SignInButton) findViewById(R.id.login_button);
        signout_button = (Button) findViewById(R.id.signout_button);
        signout_button.setOnClickListener(this);
        login_button.setOnClickListener(this);
        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this,this).addApi(Auth.GOOGLE_SIGN_IN_API,signInOptions).build();

        //profile_layout.setVisibility(View.GONE);

        if (ProfileClass.is_login()){
            login_layout.setVisibility(View.GONE);
            profile_name.setText(ProfileClass.getName());
            Glide.with(this)
                    .load(ProfileClass.getProfile_url())
                    .into(profile_image);
            profile_email.setText(ProfileClass.getEmail());
        }else {
            profile_layout.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_button :
                signIn();
                break;

            case R.id.signout_button :
                signOut();
                break;

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void signIn(){
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent,REQ_CODE);
        //Toast.makeText(this, "SIGNIN", Toast.LENGTH_SHORT).show();
    }

    public void signOut(){
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                updateUI(false);
                ProfileClass.signOut();
            }
        });
    }

    private void handleResult(GoogleSignInResult result){

        if (result.isSuccess()){
            GoogleSignInAccount account = result.getSignInAccount();
            name = account.getDisplayName();
            email = account.getEmail();
            profile_url = account.getPhotoUrl().toString();
            id_profile = account.getId();
            profile_name.setText(name);
            profile_email.setText(email);
            Glide.with(this)
                    .load(profile_url)
                    .into(this.profile_image);
            updateUI(true);
            new ProfileClass(name,email,profile_url,id_profile);
            //Toast.makeText(this, id_profile+" "+name+" "+email, Toast.LENGTH_SHORT).show();
            //Toast.makeText(this, "Login Sukses", Toast.LENGTH_SHORT).show();
            //checkUser();
            //new LongOperation().execute();
        }else {
            updateUI(false);
        }

    }

    public void checkUser(){
        new LongOperation().execute();
    }

    public void updateUI(boolean isLogin){

        if (isLogin){
            profile_layout.setVisibility(View.VISIBLE);
            login_layout.setVisibility(View.GONE);
            checkUser();
        }else {
            profile_layout.setVisibility(View.GONE);
            login_layout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE){

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);

        }
    }

    private class LongOperation extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            //Toast.makeText(getContext(), "MASUK DO IN BACKGROUND", Toast.LENGTH_SHORT).show();
            Log.e("DO IN BACKGROUND", "MASUK");

            List<Pair<String, String>> args = new ArrayList<Pair<String, String>>();
            args.add(new Pair<String, String>(TAG_ID_GOOGLE,ProfileClass.getId_profile()));
            args.add(new Pair<String, String>(TAG_NAMA_PROFILE,ProfileClass.getName()));
            args.add(new Pair<String, String>(TAG_EMAIL_PROFILE,ProfileClass.getEmail()));

            JSONObject jsonObject = null;

            try {
                jsonObject = jsonParser.makeHttpRequest(url_check_user,"POST",args);
            } catch (IOException e) {
                //e.printStackTrace();
                Log.d("Networking", e.getLocalizedMessage());
            }
            try {
                int success = jsonObject.getInt(TAG_SUCCESS);
                if (success == 1){

                }else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String jsonArray) {
            super.onPostExecute(jsonArray);
//            updateUI();
//            Intent i = new Intent(SplashScreenActivity.this,MainActivity.class);
//            i.putExtra("komik_list",komikList);
//            startActivity(i);
//            finish();
        }

    }
}
