package id.ac.umn.illuminatoon;


import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment {

    private WebView webView;

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_news,container,false);
        //WebView webView = (WebView) getView().findViewById(R.id.news_web);
        webView = (WebView) rootView.findViewById(R.id.news_web);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        Uri uri = Uri.parse(AsyncTask.base_url+"index.php/Api/getNews");
        webView.loadUrl(uri.toString());
        //webView.stopLoading();
        return rootView;
    }



}
