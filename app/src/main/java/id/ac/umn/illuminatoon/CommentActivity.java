package id.ac.umn.illuminatoon;

import android.content.Intent;
import android.os.*;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommentActivity extends AppCompatActivity {

    JSONParser jsonParser = new JSONParser();

    ArrayList<? extends HashMap<String, String>> commentList = new ArrayList<HashMap<String, String>>();

    private static String url_add_comment = AsyncTask.base_url+"index.php/Api/addComment";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_ID_CHAPTER = "id_chapter";
    private static final String TAG_ID_GOOGLE = "id_google";
    private static final String TAG_NAMA_USER = "nama_user";
    private static final String TAG_COMMENT = "comment";

    private String newCommentText;

    private EditText newComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        Button postButton = (Button) findViewById(R.id.post_comment);
        newComment = (EditText) findViewById(R.id.new_comment);
        commentList = getIntent().getParcelableArrayListExtra("commentList");
        if (!ProfileClass.is_login()){
            newComment.setEnabled(false);
            newComment.setHint("Please Login Before Commenting");
            postButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CommentActivity.this,LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }else {
            newComment.setEnabled(true);
            postButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!newComment.getText().toString().equals("")){
                        new CommentActivity.AddComment().execute();
                    }else {
                        Toast.makeText(CommentActivity.this, "Comment Box is empty", Toast.LENGTH_SHORT).show();
                        //Toast.makeText(CommentActivity.this, ProfileClass.getId_profile()+" "+newCommentText+" "+id_chapter, Toast.LENGTH_SHORT).show();
                        //Toast.makeText(CommentActivity.this, chapter.length(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        TableLayout commentSection = (TableLayout) findViewById(R.id.comment_section);

        for (int i = 0; i<commentList.size(); i++){

            TableRow commentRow = new TableRow(this);
            TableLayout.LayoutParams commentRowParam = new TableLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            commentRowParam.setMargins(
                    (int) getResources().getDimension(R.dimen.padding_recomended_row),
                    (int) getResources().getDimension(R.dimen.padding_recomended_row),
                    (int) getResources().getDimension(R.dimen.padding_recomended_row),
                    (int) getResources().getDimension(R.dimen.padding_recomended_row)
            );
            commentRow.setPadding(
                    (int) getResources().getDimension(R.dimen.padding_recomended_row),
                    (int) getResources().getDimension(R.dimen.padding_recomended_row),
                    (int) getResources().getDimension(R.dimen.padding_recomended_row),
                    (int) getResources().getDimension(R.dimen.padding_recomended_row)
            );
            commentRow.setLayoutParams(commentRowParam);
            commentRow.setBackgroundColor(getResources().getColor(R.color.colorButton));

            LinearLayout innerRow = new LinearLayout(this);
            TableRow.LayoutParams innerRowParam = new TableRow.LayoutParams(
                    0,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    1
            );
            innerRow.setOrientation(LinearLayout.VERTICAL);
            innerRow.setPadding(
                    (int) getResources().getDimension(R.dimen.padding_recomended_row),
                    (int) getResources().getDimension(R.dimen.padding_recomended_row),
                    (int) getResources().getDimension(R.dimen.padding_recomended_row),
                    (int) getResources().getDimension(R.dimen.padding_recomended_row)
            );
            innerRow.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            innerRow.setLayoutParams(innerRowParam);

            TextView titleText = new TextView(this);
            LinearLayout.LayoutParams titleTextParam = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    1
            );
            titleTextParam.setMargins(
                    0,
                    0,
                    0,
                    (int) getResources().getDimension(R.dimen.padding_recomended_row)
            );
            titleText.setLayoutParams(titleTextParam);
            titleText.setTextColor(getResources().getColor(R.color.colorAccent));
            titleText.setText(commentList.get(i).get(TAG_NAMA_USER));

            TextView commentBody = new TextView(this);
            LinearLayout.LayoutParams commentBodyParam = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    1
            );
            commentBody.setLayoutParams(commentBodyParam);
            commentBody.setTextColor(getResources().getColor(R.color.colorAccent));
            commentBody.setText(commentList.get(i).get(TAG_COMMENT));

            innerRow.addView(titleText);
            innerRow.addView(commentBody);

            commentRow.addView(innerRow);

            commentSection.addView(commentRow);

        }

    }

    private class AddComment extends android.os.AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            newCommentText = newComment.getText().toString();

        }

        @Override
        protected String doInBackground(String... params) {

            List<Pair<String, String>> args = new ArrayList<Pair<String, String>>();
            args.add(new Pair<String, String>(TAG_ID_CHAPTER, getIntent().getStringExtra("id_chapter")));
            args.add(new Pair<String, String>(TAG_COMMENT, newCommentText));
            args.add(new Pair<String, String>(TAG_ID_GOOGLE, ProfileClass.getId_profile()));

            JSONObject jsonObject = null;

            String finalSuccess = "0";

            try {
                jsonObject = jsonParser.makeHttpRequest(url_add_comment,"POST",args);
            } catch (IOException e) {
                //e.printStackTrace();
                Log.d("Networking", e.getLocalizedMessage());
            }
            try {
                int success = jsonObject.getInt(TAG_SUCCESS);
                if (success == 1){
                    finalSuccess = Integer.toString(success);
                }else {
                    finalSuccess = Integer.toString(success);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return finalSuccess;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (Integer.parseInt(s)==1){
                addNewComment();
                //pageList.clear();
                //commentList.clear();
                //longOperation.execute();
                //new LongOperation().execute();
            }
        }


    }

    public void addNewComment(){

        TableLayout commentSection = (TableLayout)findViewById(R.id.comment_section);

        TableRow commentRow = new TableRow(this);
        TableLayout.LayoutParams commentRowParam = new TableLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        commentRowParam.setMargins(
                (int) getResources().getDimension(R.dimen.padding_recomended_row),
                (int) getResources().getDimension(R.dimen.padding_recomended_row),
                (int) getResources().getDimension(R.dimen.padding_recomended_row),
                (int) getResources().getDimension(R.dimen.padding_recomended_row)
        );
        commentRow.setPadding(
                (int) getResources().getDimension(R.dimen.padding_recomended_row),
                (int) getResources().getDimension(R.dimen.padding_recomended_row),
                (int) getResources().getDimension(R.dimen.padding_recomended_row),
                (int) getResources().getDimension(R.dimen.padding_recomended_row)
        );
        commentRow.setLayoutParams(commentRowParam);
        commentRow.setBackgroundColor(getResources().getColor(R.color.colorButton));

        LinearLayout innerRow = new LinearLayout(this);
        TableRow.LayoutParams innerRowParam = new TableRow.LayoutParams(
                0,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                1
        );
        innerRow.setOrientation(LinearLayout.VERTICAL);
        innerRow.setPadding(
                (int) getResources().getDimension(R.dimen.padding_recomended_row),
                (int) getResources().getDimension(R.dimen.padding_recomended_row),
                (int) getResources().getDimension(R.dimen.padding_recomended_row),
                (int) getResources().getDimension(R.dimen.padding_recomended_row)
        );
        innerRow.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        innerRow.setLayoutParams(innerRowParam);

        TextView titleText = new TextView(this);
        LinearLayout.LayoutParams titleTextParam = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                1
        );
        titleTextParam.setMargins(
                0,
                0,
                0,
                (int) getResources().getDimension(R.dimen.padding_recomended_row)
        );
        titleText.setLayoutParams(titleTextParam);
        titleText.setTextColor(getResources().getColor(R.color.colorAccent));
        titleText.setText(ProfileClass.getName());

        TextView commentBody = new TextView(this);
        LinearLayout.LayoutParams commentBodyParam = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                1
        );
        commentBody.setLayoutParams(commentBodyParam);
        commentBody.setTextColor(getResources().getColor(R.color.colorAccent));
        commentBody.setText(newCommentText);

        innerRow.addView(titleText);
        innerRow.addView(commentBody);

        commentRow.addView(innerRow);

        commentSection.addView(commentRow);

        newComment.setText("");

    }
}
