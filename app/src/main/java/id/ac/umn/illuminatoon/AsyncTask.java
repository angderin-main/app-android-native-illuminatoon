package id.ac.umn.illuminatoon;

import android.util.Log;
import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Derin Anggara on 6/13/2017.
 */

public class AsyncTask {
    private static JSONParser jsonParser = new JSONParser();
    private static ArrayList<HashMap<String, String>> komikList = new ArrayList<HashMap<String, String>>();
    private static ArrayList<HashMap<String, String>> recomendedList = new ArrayList<HashMap<String, String>>();
    private static ArrayList<HashMap<String, String>> newestList = new ArrayList<HashMap<String, String>>();
    private static ArrayList<HashMap<String, String>> rankList = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> carouselList = new ArrayList<HashMap<String, String>>();

    public static final String base_url = "http://192.168.1.10/kuliah/pemmob/illuminatoon/project/";

    private static final String url_all_komik = base_url+"index.php/Api/getKomik";
    private static final String url_chapter_komik = base_url+"index.php/Api/getHome";
    private static final String url_carousel = base_url+"index.php/Api/getCarousel";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_KOMIK = "komik";
    private static final String TAG_ID_KOMIK = "id_komik";
    private static final String TAG_NAMA_KOMIK = "nama_komik";
    private static final String TAG_DESC = "desc";
    private static final String TAG_STATUS = "status";
    private static final String TAG_URL_PROFILE = "url_profile";

    private static final String TAG_RECOMENDED = "recomended";
    private static final String TAG_NEWEST = "newest";
    private static final String TAG_RANK = "rank";
    private static final String TAG_NAMA_PENULIS = "nama_penulis";
    private static final String TAG_TANGGAL_TERBIT = "tanggal_terbit";
    private static final String TAG_LIKE = "like_komik";

    private static final String TAG_CAROUSEL = "carousel";
    private static final String TAG_URL_CAROUSEL = "url_carousel";

    void startAsyncTask(){

        //Log.d("ASYNC TASK","MASUK FUNCTION ASYNC");
        new GetComicList().execute();
        new GetHome().execute();
        new GetCarousel().execute();

    }

    private void setCarouselList() {
        ArrayCarouselClass.setCarouselList(carouselList);
    }

    private void setListKomik(){
        ArrayComicClass.setKomikList(komikList);
    }

    private void setHome(){
        ArrayComicClass.setRecomendedList(recomendedList);
        ArrayComicClass.setRankList(rankList);
        ArrayComicClass.setNewestList(newestList);
    }

    private class GetComicList extends android.os.AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            komikList.clear();
        }

        @Override
        protected String doInBackground(String... params) {

            List<Pair<String, String>> args = new ArrayList<Pair<String, String>>();

            JSONObject jsonObject = null;

            try {
                jsonObject = jsonParser.makeHttpRequest(url_all_komik,"GET",args);
            } catch (IOException e) {
                //e.printStackTrace();
                Log.d("Networking", e.getLocalizedMessage());
            }
            try {
                int success = jsonObject.getInt(TAG_SUCCESS);
                if (success == 1){
                    JSONArray komik = jsonObject.getJSONArray(TAG_KOMIK);

                    for (int i = 0; i< komik.length(); i++){
                        JSONObject c = komik.getJSONObject(i);

                        String id_komik = c.getString(TAG_ID_KOMIK);
                        String nama_komik = c.getString(TAG_NAMA_KOMIK);
                        String desc = c.getString(TAG_DESC);
                        String status = c.getString(TAG_STATUS);
                        String tanggal_terbit = c.getString(TAG_TANGGAL_TERBIT);
                        String url_profile = c.getString(TAG_URL_PROFILE);

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID_KOMIK,id_komik);
                        map.put(TAG_NAMA_KOMIK,nama_komik);
                        map.put(TAG_DESC,desc);
                        map.put(TAG_STATUS,status);
                        map.put(TAG_TANGGAL_TERBIT,tanggal_terbit);
                        map.put(TAG_URL_PROFILE,url_profile);

                        komikList.add(map);
                    }
                }else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String jsonArray) {
            super.onPostExecute(jsonArray);
            setListKomik();
        }

    }

    private class GetHome extends android.os.AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            recomendedList.clear();
            newestList.clear();
            rankList.clear();
        }

        @Override
        protected String doInBackground(String... params) {

            Log.e("DO IN BACKGROUND", "MASUK");

            List<Pair<String, String>> args = new ArrayList<Pair<String, String>>();

            JSONObject jsonObject = null;

            try {
                jsonObject = jsonParser.makeHttpRequest(url_chapter_komik,"POST",args);
            } catch (IOException e) {
                Log.d("Networking", e.getLocalizedMessage());
            }
            try {
                int success = jsonObject.getInt(TAG_SUCCESS);
                if (success == 1){
                    JSONArray recomended = jsonObject.getJSONArray(TAG_RECOMENDED);
                    JSONArray newest = jsonObject.getJSONArray(TAG_NEWEST);
                    JSONArray rank = jsonObject.getJSONArray(TAG_RANK);

                    for (int i = 0; i< recomended.length(); i++){

                        JSONObject c = recomended.getJSONObject(i);

                        String id_komik = c.getString(TAG_ID_KOMIK);
                        String nama_komik = c.getString(TAG_NAMA_KOMIK);
                        String desc = c.getString(TAG_DESC);
                        String status = c.getString(TAG_STATUS);
                        String tanggal_terbit = c.getString(TAG_TANGGAL_TERBIT);
                        String url_profile = c.getString(TAG_URL_PROFILE);
                        String nama_penulis = c.getString(TAG_NAMA_PENULIS);
                        String like = c.getString(TAG_LIKE);

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID_KOMIK,id_komik);
                        map.put(TAG_NAMA_KOMIK,nama_komik);
                        map.put(TAG_DESC,desc);
                        map.put(TAG_STATUS,status);
                        map.put(TAG_TANGGAL_TERBIT,tanggal_terbit);
                        map.put(TAG_URL_PROFILE,url_profile);
                        map.put(TAG_NAMA_PENULIS,nama_penulis);
                        map.put(TAG_LIKE,like);

                        recomendedList.add(map);
                    }

                    for (int i = 0; i< newest.length(); i++){

                        JSONObject c = newest.getJSONObject(i);

                        String id_komik = c.getString(TAG_ID_KOMIK);
                        String nama_komik = c.getString(TAG_NAMA_KOMIK);
                        String desc = c.getString(TAG_DESC);
                        String status = c.getString(TAG_STATUS);
                        String tanggal_terbit = c.getString(TAG_TANGGAL_TERBIT);
                        String url_profile = c.getString(TAG_URL_PROFILE);
                        String nama_penulis = c.getString(TAG_NAMA_PENULIS);
                        String like = c.getString(TAG_LIKE);

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID_KOMIK,id_komik);
                        map.put(TAG_NAMA_KOMIK,nama_komik);
                        map.put(TAG_DESC,desc);
                        map.put(TAG_STATUS,status);
                        map.put(TAG_TANGGAL_TERBIT,tanggal_terbit);
                        map.put(TAG_URL_PROFILE,url_profile);
                        map.put(TAG_NAMA_PENULIS,nama_penulis);
                        map.put(TAG_LIKE,like);

                        newestList.add(map);
                    }

                    for (int i = 0; i< rank.length(); i++){

                        JSONObject c = rank.getJSONObject(i);

                        String id_komik = c.getString(TAG_ID_KOMIK);
                        String nama_komik = c.getString(TAG_NAMA_KOMIK);
                        String desc = c.getString(TAG_DESC);
                        String status = c.getString(TAG_STATUS);
                        String tanggal_terbit = c.getString(TAG_TANGGAL_TERBIT);
                        String url_profile = c.getString(TAG_URL_PROFILE);
                        String nama_penulis = c.getString(TAG_NAMA_PENULIS);
                        String like = c.getString(TAG_LIKE);

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID_KOMIK,id_komik);
                        map.put(TAG_NAMA_KOMIK,nama_komik);
                        map.put(TAG_DESC,desc);
                        map.put(TAG_STATUS,status);
                        map.put(TAG_TANGGAL_TERBIT,tanggal_terbit);
                        map.put(TAG_URL_PROFILE,url_profile);
                        map.put(TAG_NAMA_PENULIS,nama_penulis);
                        map.put(TAG_LIKE,like);

                        rankList.add(map);
                    }
                }else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String jsonArray) {

            super.onPostExecute(jsonArray);
            setHome();

        }

    }

    private class GetCarousel extends android.os.AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            carouselList.clear();
        }

        @Override
        protected String doInBackground(String... params) {

            List<Pair<String, String>> args = new ArrayList<Pair<String, String>>();

            JSONObject jsonObject = null;

            try {
                jsonObject = jsonParser.makeHttpRequest(url_carousel,"POST",args);
            } catch (IOException e) {
                //e.printStackTrace();
                Log.d("Networking", e.getLocalizedMessage());
            }
            try {
                int success = jsonObject.getInt(TAG_SUCCESS);
                if (success == 1){
                    JSONArray chapter = jsonObject.getJSONArray(TAG_CAROUSEL);

                    for (int i = 0; i< chapter.length(); i++){
                        JSONObject c = chapter.getJSONObject(i);

                        String id_komik = c.getString(TAG_ID_KOMIK);
                        String url_carousel = c.getString(TAG_URL_CAROUSEL);
                        String nama_komik = c.getString(TAG_NAMA_KOMIK);
                        String desc = c.getString(TAG_DESC);
                        String status = c.getString(TAG_STATUS);
                        String tanggal_terbit = c.getString(TAG_TANGGAL_TERBIT);
                        String url_profile = c.getString(TAG_URL_PROFILE);
                        String nama_penulis = c.getString(TAG_NAMA_PENULIS);
                        String like = c.getString(TAG_LIKE);

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID_KOMIK,id_komik);
                        map.put(TAG_URL_CAROUSEL,url_carousel);
                        map.put(TAG_NAMA_KOMIK,nama_komik);
                        map.put(TAG_DESC,desc);
                        map.put(TAG_STATUS,status);
                        map.put(TAG_TANGGAL_TERBIT,tanggal_terbit);
                        map.put(TAG_URL_PROFILE,url_profile);
                        map.put(TAG_NAMA_PENULIS,nama_penulis);
                        map.put(TAG_LIKE,like);

                        //komikList.add(map);
                        carouselList.add(map);
                    }
                }else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String jsonArray) {
            super.onPostExecute(jsonArray);
            setCarouselList();
//            updateUI();
//            Intent i = new Intent(SplashScreenActivity.this,MainActivity.class);
//            i.putExtra("komik_list",komikList);
//            startActivity(i);
//            finish();
        }

    }

}
