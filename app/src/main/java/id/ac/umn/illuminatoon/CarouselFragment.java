package id.ac.umn.illuminatoon;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class CarouselFragment extends Fragment {


    public CarouselFragment() {
        // Required empty public constructor
    }
    public static final String ARG_URL_carousel = "url_carousel";
    public static final String ARG_ID_KOMIK = "id_komik";
    public static final String ARG_NAMA_KOMIK = "nama_komik";
    public static final String ARG_DESC = "desc";
    public static final String ARG_STATUS = "status";
    public static final String ARG_URL_PROFILE = "url_profile";
    public static final String ARG_NAMA_PENULIS = "nama_penulis";
    public static final String ARG_TANGGAL_TERBIT = "tanggal_terbit";
    public static final String ARG_LIKE = "like_komik";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_carousel, container, false);
        View rootView = inflater.inflate(R.layout.fragment_carousel,container,false);
        ImageView carouselImage = (ImageView) rootView.findViewById(R.id.carousel_image);
        final Bundle bundle = getArguments();
        //String url_carousel = bundle.getString(ARG_URL_carousel);
        //final String id_komik = bundle.getString(ARG_ID_KOMIK);
        //Log.d("carousel fragment",url_carousel);
        //carouselImage.setImageResource(bundle.getInt(ARG_URL_carousel));

        carouselImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),ChapterActivity.class);
                intent.putExtra("id_komik",bundle.getString(ARG_ID_KOMIK));
                intent.putExtra("nama_komik",bundle.getString(ARG_NAMA_KOMIK));
                intent.putExtra("desc",bundle.getString(ARG_DESC));
                intent.putExtra("status",bundle.getString(ARG_STATUS));
                intent.putExtra("url_profile",bundle.getString(ARG_URL_PROFILE));
                intent.putExtra("nama_penulis",bundle.getString(ARG_NAMA_PENULIS));
                intent.putExtra("tanggal_terbit",bundle.getString(ARG_TANGGAL_TERBIT));
                intent.putExtra("like",bundle.getString(ARG_LIKE));
                startActivity(intent);
            }
        });

        Glide.with(getContext())
                .load(bundle.getString(ARG_URL_carousel))
                .placeholder(R.drawable.intro_icon)
                .into(carouselImage);
        return rootView;
    }

}
