package id.ac.umn.illuminatoon;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListComicFragment extends Fragment {

    ArrayList<HashMap<String, String>> komikList = new ArrayList<HashMap<String, String>>();

    private static final String TAG_KOMIK = "komik";
    private static final String TAG_ID_KOMIK = "id_komik";
    private static final String TAG_NAMA_KOMIK = "nama_komik";
    private static final String TAG_DESC = "desc";
    private static final String TAG_STATUS = "status";
    private static final String TAG_TANGGAL_TERBIT = "tanggal_terbit";
    private static final String TAG_URL_PROFILE = "url_profile";
    private static final String TAG_NAMA_PENULIS = "nama_penulis";
    private static final String TAG_LIKE = "like_komik";

    private View rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        rootView = inflater.inflate(R.layout.fragment_list_comic,container,false);
//        Spinner sort_spinner = (Spinner) rootView.findViewById(R.id.sort_spinner);
//        ArrayAdapter<CharSequence> sortSpinnerAdapter = ArrayAdapter.createFromResource(getContext(),
//                R.array.sort_array,
//                android.R.layout.simple_spinner_item
//        );
//        sortSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        sort_spinner.setAdapter(sortSpinnerAdapter);

        komikList = ArrayComicClass.getKomikList();

        updateUI();

//        TextView search = (TextView) rootView.findViewById(R.id.search_text);
//        search.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });

        return rootView;
    }

    public void updateUI(){

        LinearLayout linearLayout = (LinearLayout) rootView.findViewById(R.id.komik_list);

        int count = 0;

        for (int i=0; i<((int) Math.ceil(komikList.size()/2.0)); i++){
            TableRow tableRow = new TableRow(getContext());
            TableLayout.LayoutParams tableRowParams = new TableLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    (int) getResources().getDimension(R.dimen.height_recomended_row)
            );
            tableRowParams.setMargins(
                    0,
                    (int)getResources().getDimension(R.dimen.padding_recomended_row),
                    0,
                    (int)getResources().getDimension(R.dimen.padding_recomended_row)
            );
            tableRow.setLayoutParams(tableRowParams);
            tableRow.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            for (int x=0; x<2; x++,count++){
                //komikList.get(count).get(TAG_NAMA_KOMIK);
                if (count==komikList.size())break;
                LinearLayout linearLayout2 = new LinearLayout(getContext());
                linearLayout2.setLayoutParams(new TableRow.LayoutParams(
                        0,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        1
                ));
                linearLayout2.setOrientation(LinearLayout.VERTICAL);
                linearLayout2.setGravity(Gravity.CENTER);
                linearLayout2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                FrameLayout frameLayout = new FrameLayout(getContext());
                frameLayout.setLayoutParams(new LinearLayout.LayoutParams(
                        (int)getResources().getDimension(R.dimen.height_recomended_row),
                        (int)getResources().getDimension(R.dimen.height_recomended_row)
                ));

                ImageView profileImage = new ImageView(getContext());
                profileImage.setLayoutParams(new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                ));
                Glide.with(getContext())
                        .load(komikList.get(count).get(TAG_URL_PROFILE))
                        .into(profileImage);

                ImageView blackOverlay = new ImageView(getContext());
                blackOverlay.setLayoutParams(new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                ));
                blackOverlay.setImageDrawable(getResources().getDrawable(R.drawable.black_overlay));

                TextView nama_komik = new TextView(getContext());
                nama_komik.setLayoutParams(new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                ));
                nama_komik.setGravity(Gravity.CENTER|Gravity.BOTTOM);
                nama_komik.setPadding(0,0,0, (int) getResources().getDimension(R.dimen.padding_recomended_row));
                nama_komik.setTextColor(getResources().getColor(R.color.colorAccent));
                nama_komik.setText(komikList.get(count).get(TAG_NAMA_KOMIK));
                nama_komik.setId(count);
                nama_komik.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(),ChapterActivity.class);
                        intent.putExtra("id_komik",komikList.get(v.getId()).get(TAG_ID_KOMIK));
                        intent.putExtra("url_profile",komikList.get(v.getId()).get(TAG_URL_PROFILE));
                        intent.putExtra("desc",komikList.get(v.getId()).get(TAG_DESC));
                        intent.putExtra("nama_komik",komikList.get(v.getId()).get(TAG_NAMA_KOMIK));
                        intent.putExtra("nama_penulis",komikList.get(v.getId()).get(TAG_NAMA_PENULIS));
                        intent.putExtra("tanggal_terbit",komikList.get(v.getId()).get(TAG_TANGGAL_TERBIT));
                        intent.putExtra("like",komikList.get(v.getId()).get(TAG_LIKE));
                        startActivity(intent);
                    }
                });

                frameLayout.addView(profileImage);
                frameLayout.addView(blackOverlay);
                frameLayout.addView(nama_komik);

                linearLayout2.addView(frameLayout);

                tableRow.addView(linearLayout2);

            }

            linearLayout.addView(tableRow);
        }

    }

}
