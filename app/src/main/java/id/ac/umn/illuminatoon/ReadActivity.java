package id.ac.umn.illuminatoon;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ReadActivity extends AppCompatActivity {

    JSONParser jsonParser = new JSONParser();
    ArrayList<HashMap<String, String>> pageList = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> commentList = new ArrayList<HashMap<String, String>>();

    private static String url_page_komik = id.ac.umn.illuminatoon.AsyncTask.base_url+"index.php/Api/getPage";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PAGE = "page";
    private static final String TAG_COMMENTS = "comments";
    private static final String TAG_NAMA_USER = "nama_user";
    private static final String TAG_COMMENT = "comment";
    private static final String TAG_ID = "id";
    private static final String TAG_ID_CHAPTER = "id_chapter";
    private static final String TAG_URL_PAGE = "url_page";

    JSONArray chapter, comments = null;

    private String id_chapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);

        id_chapter = getIntent().getStringExtra("id_chapter");
        Button postButton = (Button) findViewById(R.id.post_comment);
        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReadActivity.this,CommentActivity.class);
                intent.putExtra("commentList",commentList);
                intent.putExtra("id_chapter",id_chapter);
                startActivity(intent);
            }
        });

        new LongOperation().execute();
    }

    public void updateUI(){
        int count = 0;

        Button dummy = (Button)findViewById(R.id.dummy);
        dummy.requestFocus();

        LinearLayout pageView = (LinearLayout)findViewById(R.id.page_view);
        TableLayout commentSection = (TableLayout)findViewById(R.id.comment_section);

        for (int i=0; i<pageList.size(); i++){
            Log.d("Page",pageList.get(i).get(TAG_URL_PAGE));
            ImageView page = new ImageView(this);
            page.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            ));
            Glide.with(this)
                    .load(pageList.get(i).get(TAG_URL_PAGE))
                    .into(page);
            pageView.addView(page);
        }

    }

    private class LongOperation extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!pageList.isEmpty())pageList.clear();
            if (!commentList.isEmpty())commentList.clear();
            if (chapter!=null)chapter = null;
            if (comments!=null)comments = null;
        }

        @Override
        protected String doInBackground(String... params) {

            List<Pair<String, String>> args = new ArrayList<Pair<String, String>>();
            args.add(new Pair<String, String>(TAG_ID_CHAPTER, id_chapter));

            JSONObject jsonObject = null;

            try {
                jsonObject = jsonParser.makeHttpRequest(url_page_komik,"POST",args);
            } catch (IOException e) {
                //e.printStackTrace();
                Log.d("Networking", e.getLocalizedMessage());
            }
            try {
                int success = jsonObject.getInt(TAG_SUCCESS);
                if (success == 1){
                    chapter = jsonObject.getJSONArray(TAG_PAGE);
                    comments = jsonObject.getJSONArray(TAG_COMMENTS);

                    for (int i=0; i<chapter.length(); i++){
                        JSONObject c = chapter.getJSONObject(i);

                        String id_page = c.getString(TAG_ID);
                        String url_page = c.getString(TAG_URL_PAGE);

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID,id_page);
                        map.put(TAG_URL_PAGE,url_page);

                        //komikList.add(map);
                        pageList.add(map);
                    }

                    for (int i = 0; i<comments.length(); i++){
                        JSONObject c = comments.getJSONObject(i);

                        String nama_user = c.getString(TAG_NAMA_USER);
                        String comment = c.getString(TAG_COMMENT);
                        //Date date = ;

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_NAMA_USER, nama_user);
                        map.put(TAG_COMMENT, comment);

                        commentList.add(map);
                    }
                }else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String jsonArray) {
            super.onPostExecute(jsonArray);
            updateUI();
        }

    }

}
