package id.ac.umn.illuminatoon;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Derin Anggara on 4/30/2017.
 */

public class CarouselPagerAdapter extends FragmentStatePagerAdapter {

    JSONParser jsonParser = new JSONParser();
    private ArrayList<HashMap<String, String>> carouselList = new ArrayList<HashMap<String, String>>();

    private static String url_carousel = "http://192.168.1.56/kuliah/pemmob/illuminatoon/project/index.php/Api/getCarousel";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_CAROUSEL = "carousel";
    private static final String TAG_ID_KOMIK = "id_komik";
    private static final String TAG_URL_CAROUSEL = "url_carousel";
    private static final String TAG_NAMA_KOMIK = "nama_komik";
    private static final String TAG_DESC = "desc";
    private static final String TAG_STATUS = "status";
    private static final String TAG_URL_PROFILE = "url_profile";
    private static final String TAG_NAMA_PENULIS = "nama_penulis";
    private static final String TAG_TANGGAL_TERBIT = "tanggal_terbit";
    private static final String TAG_LIKE = "like_komik";

    JSONArray chapter = null;

    private String id_chapter;

    public CarouselPagerAdapter(FragmentManager fm) {
        super(fm);
        carouselList = ArrayCarouselClass.getCarouselList();
//        HashMap<String, String> map = new HashMap<String, String>();
//        map.put(TAG_URL_CAROUSEL,"http://192.168.0.106/kuliah/pemmob/illuminatoon/project/assets/uploads/carousel/11e69-1.png");
//        map.put(TAG_ID_KOMIK,"1");
//        carouselList.add(map);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new CarouselFragment();
        Bundle bundle = new Bundle();
        bundle.putString(CarouselFragment.ARG_URL_carousel,carouselList.get(position).get(TAG_URL_CAROUSEL));
        bundle.putString(CarouselFragment.ARG_ID_KOMIK,carouselList.get(position).get(TAG_ID_KOMIK));
        bundle.putString(CarouselFragment.ARG_NAMA_KOMIK,carouselList.get(position).get(TAG_NAMA_KOMIK));
        bundle.putString(CarouselFragment.ARG_DESC,carouselList.get(position).get(TAG_DESC));
        bundle.putString(CarouselFragment.ARG_STATUS,carouselList.get(position).get(TAG_STATUS));
        bundle.putString(CarouselFragment.ARG_URL_PROFILE,carouselList.get(position).get(TAG_URL_PROFILE));
        bundle.putString(CarouselFragment.ARG_NAMA_PENULIS,carouselList.get(position).get(TAG_NAMA_PENULIS));
        bundle.putString(CarouselFragment.ARG_TANGGAL_TERBIT,carouselList.get(position).get(TAG_TANGGAL_TERBIT));
        bundle.putString(CarouselFragment.ARG_LIKE,carouselList.get(position).get(TAG_LIKE));
        fragment.setArguments(bundle);
        return fragment;
    }



    @Override
    public int getCount() {
        return carouselList.size();
    }

}
