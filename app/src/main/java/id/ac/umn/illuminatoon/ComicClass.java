package id.ac.umn.illuminatoon;

/**
 * Created by Derin Anggara on 6/13/2017.
 */

public class ComicClass {

    String id_komik;
    String nama_komik;
    String desc;
    String status;
    String tanggal_terbit;
    String url_profile;
    String nama_penulis;


    public ComicClass(String id_komik, String nama_komik, String desc, String status, String tanggal_terbit, String url_profile, String nama_penulis) {
        this.id_komik = id_komik;
        this.nama_komik = nama_komik;
        this.desc = desc;
        this.status = status;
        this.tanggal_terbit = tanggal_terbit;
        this.url_profile = url_profile;
        this.nama_penulis = nama_penulis;
    }

    public String getId_komik() {
        return id_komik;
    }

    public void setId_komik(String id_komik) {
        this.id_komik = id_komik;
    }

    public String getNama_komik() {
        return nama_komik;
    }

    public void setNama_komik(String nama_komik) {
        this.nama_komik = nama_komik;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTanggal_terbit() {
        return tanggal_terbit;
    }

    public void setTanggal_terbit(String tanggal_terbit) {
        this.tanggal_terbit = tanggal_terbit;
    }

    public String getUrl_profile() {
        return url_profile;
    }

    public void setUrl_profile(String url_profile) {
        this.url_profile = url_profile;
    }

    public String getNama_penulis() {
        return nama_penulis;
    }

    public void setNama_penulis(String nama_penulis) {
        this.nama_penulis = nama_penulis;
    }
}
