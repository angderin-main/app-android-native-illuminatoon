package id.ac.umn.illuminatoon;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class UmnScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_umn_screen);
        startLoadData();
    }

    private void startLoadData(){
        new LongOperation().execute();
    }

    private class LongOperation extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {


            for (int i = 0; i < 3
                    ; i++){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Thread.interrupted();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String jsonArray) {
            super.onPostExecute(jsonArray);
            Intent i = new Intent(UmnScreenActivity.this,MainActivity.class);
//            i.putExtra("komik_list",komikList);
            startActivity(i);
            finish();
        }

    }
}
