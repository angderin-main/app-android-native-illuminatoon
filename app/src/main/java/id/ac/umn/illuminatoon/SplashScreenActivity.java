package id.ac.umn.illuminatoon;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SplashScreenActivity extends AppCompatActivity {

//    JSONParser jsonParser = new JSONParser();
//    ArrayList<HashMap<String, String>> komikList = new ArrayList<HashMap<String, String>>();
//
//    private static String url_all_komik = "http://192.168.1.47/kuliah/pemmob/illuminatoon/project/index.php/Api/getKomik";
//
//    private static final String TAG_SUCCESS = "success";
//    private static final String TAG_KOMIK = "komik";
//    private static final String TAG_NAMA_KOMIK = "nama_komik";
//    private static final String TAG_DESC = "desc";
//    private static final String TAG_STATUS = "status";
//    private static final String TAG_TANGGAL_TERBIT = "tanggal_terbit";
//    private static final String TAG_URL_PROFILE = "url_profile";
//
//    JSONArray komik = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        startLoadData();
    }

    private void startLoadData(){
        new LongOperation().execute();
    }

    private class LongOperation extends AsyncTask<String, String, String>{

        @Override
        protected String doInBackground(String... params) {

            new id.ac.umn.illuminatoon.AsyncTask().startAsyncTask();

            for (int i = 0; i < 3
                    ; i++){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Thread.interrupted();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String jsonArray) {
            super.onPostExecute(jsonArray);
            Intent i = new Intent(SplashScreenActivity.this,UmnScreenActivity.class);
//            i.putExtra("komik_list",komikList);
            startActivity(i);
            finish();
        }

    }
}
