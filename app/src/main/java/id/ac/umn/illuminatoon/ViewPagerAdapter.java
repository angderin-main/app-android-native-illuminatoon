package id.ac.umn.illuminatoon;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Derin Anggara on 4/30/2017.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    static private ArrayList<HashMap<String,String>> komik_list = new ArrayList<HashMap<String,String>>();
    static private Bundle komik_list_bundle = new Bundle();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void getData(ArrayList<HashMap<String,String>> komik_list){
        this.komik_list = komik_list;
        komik_list_bundle.putSerializable("komil_list",this.komik_list);

    }

    @Override
    public Fragment getItem(int position) {
        //return null;
        switch (position){
            case 0 : return new HomeFragment();
            case 1 :
                Fragment fragment = new ListComicFragment();
                fragment.setArguments(komik_list_bundle);
                return fragment;
            case 2 : return new NewsFragment();
//            case 3 : return new MyFragment();
            case 3 : return new MoreFragment();
            default: return new HomeFragment();
        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}
