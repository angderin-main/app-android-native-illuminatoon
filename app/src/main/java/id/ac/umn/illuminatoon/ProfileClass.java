package id.ac.umn.illuminatoon;

/**
 * Created by Derin Anggara on 5/23/2017.
 */

public class ProfileClass {

    private static String name;
    private static String email;
    private static String profile_url;
    private static String id_profile;
    private static boolean is_login = false;

    public ProfileClass(String name, String email, String profile_url, String id_profile) {

        this.name = name;
        this.email = email;
        this.profile_url = profile_url;
        this.id_profile = id_profile;
        this.is_login = true;

    }

    public static String getName() {
        return name;
    }

    public static String getEmail() {
        return email;
    }

    public static String getProfile_url() {
        return profile_url;
    }

    public static String getId_profile() {
        return id_profile;
    }

    public static boolean is_login() {
        return is_login;
    }

    public static void signOut(){
        name = null;
        email = null;
        profile_url = null;
        id_profile = null;
        is_login = false;
    }


}
