package id.ac.umn.illuminatoon;

import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Derin Anggara on 6/13/2017.
 */

public class ArrayComicClass {

    private static ArrayList<HashMap<String, String>> komikList = new ArrayList<HashMap<String, String>>();
    private static ArrayList<HashMap<String, String>> recomendedList = new ArrayList<HashMap<String, String>>();
    private static ArrayList<HashMap<String, String>> newestList = new ArrayList<HashMap<String, String>>();
    private static ArrayList<HashMap<String, String>> rankList = new ArrayList<HashMap<String, String>>();

    public static ArrayList<HashMap<String, String>> getKomikList() {
        return komikList;
    }

    public static void setKomikList(ArrayList<HashMap<String, String>> komikList) {
        ArrayComicClass.komikList = komikList;
    }

    static ArrayList<HashMap<String, String>> getRecomendedList() {
        return recomendedList;
    }

    static void setRecomendedList(ArrayList<HashMap<String, String>> recomendedList) {
        ArrayComicClass.recomendedList = recomendedList;
    }

    static ArrayList<HashMap<String, String>> getNewestList() {
        return newestList;
    }

    static void setNewestList(ArrayList<HashMap<String, String>> newestList) {
        ArrayComicClass.newestList = newestList;
    }

    static ArrayList<HashMap<String, String>> getRankList() {
        return rankList;
    }

    static void setRankList(ArrayList<HashMap<String, String>> rankList) {
        ArrayComicClass.rankList = rankList;
    }
}
